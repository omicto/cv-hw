import React from "react";
import logo from "./logo.svg";
import "./App.css";
import "../node_modules/bootstrap/scss/bootstrap.scss";
import MainTimeline from "./components/main-timeline/timeline.component";
import SkillsCard from "./components/skills/skills-card.component";
import CvSection from "./components/cv-section/cv-section.component";
import Header from "./components/header/header.component";

//FIXME: Remove this!
const PLACE_HOLDER_PP = "https://via.placeholder.com/300";

function App() {
  return (
    <div className="bg-light">
      <Header imageUrl={PLACE_HOLDER_PP} />
      <div className="container">
        <CvSection title="skills">
          <SkillsCard />
        </CvSection>
        <CvSection title="education & employment">
          <MainTimeline />
        </CvSection>
      </div>
    </div>
  );
}

export default App;
