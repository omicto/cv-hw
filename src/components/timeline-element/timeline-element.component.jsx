import React from "react";

const TimelineElement = ({ year, title, children }) => {
  return (
    <div className="timeline">
      <div href="#" className="timeline-content">
        <div className="timeline-year">
          <span>{year}</span>
        </div>
        <h3 className="title">{title}</h3>
        <div className="timeline-icon">
          <i className="fa fa-rocket"></i>
        </div>
        <p className="description">{children}</p>
      </div>
    </div>
  );
};

export default TimelineElement;
