import React from "react";
import "./timeline.styles.scss";
import TimelineElement from "../timeline-element/timeline-element.component";

const MainTimeline = () => {
  return (
    <div class="container">
      <div class="row bg-light">
        <div class="col-md-12">
          <div class="main-timeline">
            <TimelineElement title="INTERNSHIP" year={2019}>
              Software development internship at 1A Auto, Mexico. Duties
              included manual and automated testing of the company's websites
              and the mantainance of internal tools.
            </TimelineElement>
            <TimelineElement title="INTENSIVE TRAINING PROGRAM" year={2020}>
              Participant of the 2nd generation of the Gamma - ProKarma
              Intensive Training program @ Chihuahua, Mexico. The program covers
              both front and backend concepts, with technologies such as JAX-RS,
              Core Java, Spring Framework, React, Node.JS
            </TimelineElement>
            <TimelineElement title="Bs IN COMPUTER SCIENCE" year={2020}>
              Soon to graduate from the Universidad Autónoma de Chihuahua
              program in Computer Science. Coursework included topics ranging
              from algorithms and data structures, operative systems to
              networking and web development.
            </TimelineElement>
          </div>
        </div>
      </div>
    </div>
  );
};

export default MainTimeline;
