import React from "react";

import "./header.styles.scss";

const Header = ({ imageUrl }) => {
  return (
    <div className="header">
      <div className="container overflow-hidden">
        <div className="row px-3">
          <div
            id="profile-picture"
            className="col col-md-4 cv-image p-0"
            style={{
              backgroundImage: `url(${imageUrl})`
            }}
          ></div>
          <div className="col col-md-8 px-0">
            <div className="card h-100 w-100">
              <div className="card-body">
                <h3>
                  I'm <span className="text-info">Sebastian de la Riva</span>!
                </h3>
                <h6>Computer Science Student</h6>
                <hr />
                <dl className="row">
                  <dt className="col-sm-3">Age</dt>
                  <dd className="col-sm-9">22</dd>
                  <dt className="col-sm-3">Email</dt>
                  <dd className="col-sm-9">tevas.dlr@gmail.com</dd>
                  <dt className="col-sm-3">Phone Number</dt>
                  <dd className="col-sm-9">+52 6143455232</dd>
                </dl>
              </div>
              <div className="card-footer bg-info"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Header;
