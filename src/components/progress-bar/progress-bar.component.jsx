import React from "react";

const ProgressBar = ({ percentage }) => {
  return (
    <div class="progress my-1" style={{ height: "6px" }}>
      <div
        class="progress-bar"
        role="progressbar"
        style={{ width: `${percentage}%` }}
        aria-valuenow="25"
        aria-valuemin="0"
        aria-valuemax="100"
      ></div>
    </div>
  );
};

export default ProgressBar;
