import React from "react";
import ProgressBar from "../progress-bar/progress-bar.component";

const SkillsCard = () => {
  return (
    <div className="row">
      <div className="col">
        <p>
          I'm a passionate learner and enjoy working with new and challenging
          technologies. Always trying my best to produce clean, mantainable and
          efficient solutions.
        </p>
      </div>
      <div className="col">
        <h5>Level of familiarity</h5>
        Java
        <ProgressBar percentage={80} />
        <div className="row">
          <div className="col-md-1"></div>
          <div className="col-md-11">
            Spring Framework
            <ProgressBar percentage={65} />
            JUnit
            <ProgressBar percentage={80} />
          </div>
        </div>
        Javascript
        <ProgressBar percentage={60} />
        Git
        <ProgressBar percentage={70} />
      </div>
    </div>
  );
};

export default SkillsCard;
