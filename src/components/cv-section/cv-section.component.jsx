import React, { Fragment } from "react";
import "./cv-section.styles.scss";

const CvSection = ({ title, children }) => {
  return (
    <div className="cv-section">
      <h3 className="title m-4">{title}</h3>
      <div className="card cv-section">
        <div className="card-body">{children}</div>
      </div>
    </div>
  );
};

export default CvSection;
